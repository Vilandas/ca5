package com.dkit.gd2.vilandasmorrissey.Daos;

import com.dkit.gd2.vilandasmorrissey.BusinessObjects.*;
import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;
import com.dkit.gd2.vilandasmorrissey.Exceptions.DaoException;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.util.*;

import static org.junit.Assert.*;

public class MySQLTollsDAOTest
{
    private ITollsDaoInterface ITollsDao;

    @Before
    public void setup() throws Exception
    {
        ITollsDao = new MySQLTollsDAO();
    }

    @Test
    public void getAllTollEvents() throws DaoException
    {
        VehicleRegistrations registrations = new VehicleRegistrations();
        registrations.loadRegistrations();
        TollEvents tolls = new TollEvents();
        tolls.loadTollEvents(registrations);
        List<TollEvent> expected = new ArrayList();

        for(HashSet<TollEvent> tollSet : tolls.values())
        {
            expected.addAll(tollSet);
        }
        List<TollEvent> output = ITollsDao.getAllTollEvents();
        expected.sort(new TollEventComparatorByImageID());
        output.sort(new TollEventComparatorByImageID());

        assertEquals(expected.toString(), output.toString());
    }

    @Test
    public void getTollsFromReg() throws DaoException
    {
        List<TollEvent> expected = new ArrayList();
        expected.add(new TollEvent("191LH1111", 30402, Instant.parse("2020-02-14T10:15:30Z"), Enums.Country.IRELAND));
        expected.add(new TollEvent("191LH1111", 30411, Instant.parse("2020-02-14T23:15:39Z"), Enums.Country.IRELAND));
        expected.add(new TollEvent("191LH1111", 30421, Instant.parse("2020-02-16T11:16:49Z"), Enums.Country.IRELAND));
        expected.add(new TollEvent("191LH1111", 30432, Instant.parse("2020-02-17T13:20:01Z"), Enums.Country.IRELAND));

        List<TollEvent> output = ITollsDao.getTollsFromReg("191LH1111");

        assertEquals(expected.toString(), output.toString());
    }

    @Test
    public void getTollsFromDate() throws DaoException
    {
        List<TollEvent> expected = new ArrayList();
        expected.add(new TollEvent("181MH3459", 30439, Instant.parse("2020-02-17T18:58:08Z"), Enums.Country.IRELAND));
        expected.add(new TollEvent("181XX3460", 30440, Instant.parse("2020-02-17T23:20:09Z"), Enums.Country.UNKNOWN));
        expected.add(new TollEvent("181MH3461", 30441, Instant.parse("2020-02-17T23:25:10Z"), Enums.Country.IRELAND));

        Menu menu = new Menu();
        Instant startDate = menu.getInstantFromString("2020-02-17 18:58:08");
        List<TollEvent> output = ITollsDao.getTollsFromDateRange(startDate, Instant.now());

        assertEquals(expected.toString(), output.toString());
    }

    @Test
    public void getTollsFromDateRange() throws DaoException
    {
        List<TollEvent> expected = new ArrayList();
        expected.add(new TollEvent("201LH312", 30431, Instant.parse("2020-02-16T11:16:59Z"), Enums.Country.IRELAND));
        expected.add(new TollEvent("191LH1111", 30432, Instant.parse("2020-02-17T13:20:01Z"), Enums.Country.IRELAND));
        expected.add(new TollEvent("201CN3456", 30433, Instant.parse("2020-02-17T14:25:02Z"), Enums.Country.IRELAND));

        Menu menu = new Menu();
        Instant startDate = menu.getInstantFromString("2020-02-16 11:16:59");
        Instant endDate = menu.getInstantFromString("2020-02-17 14:25:02");
        List<TollEvent> output = ITollsDao.getTollsFromDateRange(startDate, endDate);

        assertEquals(expected.toString(), output.toString());
    }

//    @Test
//    public void getTollsUniqueReg() throws DaoException
//    {
//        VehicleRegistrations registrations = new VehicleRegistrations();
//        registrations.loadRegistrations();
//        TollEvents tollEvents = new TollEvents();
//        tollEvents.loadTollEvents(registrations);
//
//        List<TollEvent> expected = new ArrayList();
//
//        for(HashSet<TollEvent> tollSet : tollEvents.values())
//        {
//            expected.addAll(tollSet);
//        }
//        expected.sort(new TollEventComparatorByImageID());
//        for(int i = expected.size() - 1; i >= 1; i--)
//        {
//            if(expected.get(i - 1).getImageID() == expected.get(i).getImageID())
//            {
//                expected.remove(i);
//            }
//        }
//
//        List<TollEvent> output = ITollsDao.getTollsUniqueReg();
//        output.sort(new TollEventComparatorByImageID());
//        assertEquals(expected.toString(), output.toString());
//    }

    @Test
    public void getAllTollsAsMap() throws DaoException
    {
        VehicleRegistrations registrations = new VehicleRegistrations();
        registrations.loadRegistrations();
        TollEvents tollEvents = new TollEvents();
        tollEvents.loadTollEvents(registrations);

        Map<String, List<TollEvent>> expected = new HashMap();

        for(String reg : tollEvents.keySet())
        {
            List tolls = new ArrayList();
            for (TollEvent tollEvent : tollEvents.get(reg))
            {
                tolls.add(tollEvent);
            }
            tolls.sort(new TollEventComparatorByImageID());
            expected.put(reg, tolls);
        }

        Map<String, List<TollEvent>> output = new HashMap();
        output.putAll(ITollsDao.getAllTollsAsMap());

        assertEquals(expected.toString(), output.toString());
    }
}