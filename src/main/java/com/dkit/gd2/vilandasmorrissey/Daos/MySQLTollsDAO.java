package com.dkit.gd2.vilandasmorrissey.Daos;

import com.dkit.gd2.vilandasmorrissey.BusinessObjects.Enums;
import com.dkit.gd2.vilandasmorrissey.BusinessObjects.TollEvents;
import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;
import com.dkit.gd2.vilandasmorrissey.Exceptions.DaoException;

import java.sql.*;
import java.time.Instant;
import java.util.*;

public class MySQLTollsDAO extends MySqlDao implements ITollsDaoInterface
{
    @Override
    public void uploadTollEvents(TollEvents allTollEvents) throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;

        try
        {
            con = this.getConnection();

            for(HashSet<TollEvent> tollSet : allTollEvents.values())
            {
                for(TollEvent tollEvent : tollSet)
                {
                    String query = "insert into Toll_Events(image_id, vehicle_registration, time, country) VALUES (?, ?, ?, ?)";
                    ps = con.prepareStatement(query);
                    ps.setLong(1, tollEvent.getImageID());
                    ps.setString(2, tollEvent.getVehicleRegistration());
                    ps.setTimestamp(3, Timestamp.from(tollEvent.getTime()));
                    ps.setString(4, tollEvent.getCountry().toString());
                    ps.executeUpdate();
                }
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("uploadTollEvents() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("uploadTollEvents() " + e.getMessage());
            }
        }
    }

    @Override
    public List<TollEvent> getAllTollEvents() throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList();

        try
        {
            con = this.getConnection();

            String query = "select * from toll_events";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                int imageId = rs.getInt("image_id");
                String registration = rs.getString("vehicle_registration");
                Instant time = (rs.getTimestamp("time")).toInstant();
                String countryString = rs.getString("country").replaceAll(" ", "_");
                Enums.Country country = Enums.Country.valueOf(countryString);
                TollEvent toll = new TollEvent(registration, imageId, time, country);
                tollEvents.add(toll);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getAllTollEvents() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getAllTollEvents() " + e.getMessage());
            }
        }
        return tollEvents;
    }

    @Override
    public List<TollEvent> getTollsFromReg(String registration) throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList();

        try
        {
            con = this.getConnection();

            String query = "select image_id, time, country from toll_events where vehicle_registration = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, registration);

            rs = ps.executeQuery();

            while(rs.next())
            {
                int imageId = rs.getInt("image_id");
                Instant time = rs.getTimestamp("time").toInstant();
                Enums.Country country = Enums.Country.valueOf(rs.getString("country"));
                TollEvent toll = new TollEvent(registration, imageId, time, country);
                tollEvents.add(toll);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getTollsFromReg() " + e.getMessage());
        }
        finally
        {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getTollsFromReg() " + e.getMessage());
            }

        }
        return tollEvents;
    }

    @Override
    public List<TollEvent> getTollsFromDateRange(Instant startTime, Instant endTime) throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList();

        try
        {
            con = this.getConnection();

            String query = "select * from toll_events where time >= ? and time <= ?";
            ps = con.prepareStatement(query);
            ps.setTimestamp(1, Timestamp.from(startTime));
            ps.setTimestamp(2, Timestamp.from(endTime));

            rs = ps.executeQuery();

            while(rs.next())
            {
                int imageId = rs.getInt("image_id");
                String registration = rs.getString("vehicle_registration");
                Instant time = rs.getTimestamp("time").toInstant();
                Enums.Country country = Enums.Country.valueOf(rs.getString("country"));
                TollEvent toll = new TollEvent(registration, imageId, time, country);
                tollEvents.add(toll);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getTollsFromDateRange() " + e.getMessage());
        }
        finally
        {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getTollsFromDateRange() " + e.getMessage());
            }

        }
        return tollEvents;
    }

    @Override
    public List<TollEvent> getTollsUniqueReg() throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList();

        try
        {
            con = this.getConnection();

            String query = "select * from toll_events group by vehicle_registration";
            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while(rs.next())
            {
                int imageId = rs.getInt("image_id");
                String registration = rs.getString("vehicle_registration");
                Instant time = rs.getTimestamp("time").toInstant();
                Enums.Country country = Enums.Country.valueOf(rs.getString("country"));
                TollEvent toll = new TollEvent(registration, imageId, time, country);
                tollEvents.add(toll);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getTollsUniqueReg() " + e.getMessage());
        }
        finally
        {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getTollsUniqueReg() " + e.getMessage());
            }

        }
        return tollEvents;
    }

    @Override
    public Map<String, List<TollEvent>> getAllTollsAsMap() throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Map<String, List<TollEvent>> allTollEvents = new HashMap();

        try
        {
            con = this.getConnection();

            String query = "select vehicle_registration from toll_events";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                String registration = rs.getString("vehicle_registration");

                List<TollEvent> tollEvents = getTollsFromReg(registration);
                allTollEvents.put(registration, tollEvents);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getAllTollEvents() " + e.getMessage());
        }
        finally
        {
            try
            {
                if(rs != null)
                {
                    rs.close();
                }
                if(ps != null)
                {
                    ps.close();
                }
                if(con != null)
                {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getAllTollEvents() " + e.getMessage());
            }
        }
        return allTollEvents;
    }

    @Override
    public List<TollEvent> getTollsFromCountry(Enums.Country country) throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<TollEvent> tollEvents = new ArrayList();

        try
        {
            con = this.getConnection();

            String query = "select image_id, vehicle_registration, time from toll_events where country = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, country.toString());

            rs = ps.executeQuery();

            while(rs.next())
            {
                int imageId = rs.getInt("image_id");
                String registration = rs.getString("vehicle_registration");
                Instant time = rs.getTimestamp("time").toInstant();
                TollEvent toll = new TollEvent(registration, imageId, time, country);
                tollEvents.add(toll);
            }
        }
        catch(SQLException e)
        {
            throw new DaoException("getTollsFromCountry() " + e.getMessage());
        }
        finally
        {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("getTollsFromCountry() " + e.getMessage());
            }

        }
        return tollEvents;
    }

    @Override
    public void clearEvents() throws DaoException
    {
        Connection con = null;
        PreparedStatement ps = null;

        try
        {
            con = this.getConnection();

            String query = "delete from toll_events";
            ps = con.prepareStatement(query);
            ps.executeUpdate();
        }
        catch(SQLException e)
        {
            throw new DaoException("clearEvents() " + e.getMessage());
        }
        finally
        {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            }
            catch(SQLException e)
            {
                throw new DaoException("clearEvents() " + e.getMessage());
            }

        }
    }
}
