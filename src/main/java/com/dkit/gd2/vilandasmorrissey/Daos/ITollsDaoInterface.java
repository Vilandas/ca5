package com.dkit.gd2.vilandasmorrissey.Daos;
/* Declares the methods that all tollsDAO types must implement
they could be mySQL, Mongo, Postgres

Classes in my Business Layer should use a reference to the interface type
to avoid dependencies on the underlying concrete classes
 */

import com.dkit.gd2.vilandasmorrissey.BusinessObjects.Enums;
import com.dkit.gd2.vilandasmorrissey.BusinessObjects.TollEvents;
import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;
import com.dkit.gd2.vilandasmorrissey.Exceptions.DaoException;

import java.time.Instant;
import java.util.*;

public interface ITollsDaoInterface
{
    public void uploadTollEvents(TollEvents tollEvents) throws DaoException;
    public List<TollEvent> getAllTollEvents() throws DaoException;
    public List<TollEvent> getTollsFromReg(String registration) throws DaoException;
    public List<TollEvent> getTollsFromDateRange(Instant startTime, Instant endTime) throws DaoException;
    public List<TollEvent> getTollsUniqueReg() throws DaoException;
    public Map<String, List<TollEvent>> getAllTollsAsMap() throws DaoException;
    public List<TollEvent> getTollsFromCountry(Enums.Country country) throws DaoException;
    public void clearEvents() throws DaoException;

}
