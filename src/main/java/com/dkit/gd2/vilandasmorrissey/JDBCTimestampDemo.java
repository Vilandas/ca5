/*  JDBCTimestampDemo                               DL Feb 2020

Writes a java.time.Instant to a TIMESTAMP field in an MySql Database.
java.time.Instant().now() should be use for obtaining the current timestamp.

Demonstrates:
- creating a java.time.Instant from a String
- converting an java.time.Instant to a java.sql.Timestamp
   (for inserting into Database TIMESTAMP field)
- reading a Timestamp from a Database and converting it to an Instant
   and then to a ZonedDateTime

It is Very important to understand that:
- A java.sql.Timestamp is in UTC by definition.
    (UTC, is GMT, i.e. London Timezone, offset 0)
- A java.time.Instant  is in UTC by definition.

Timestamps are stored in UTC time format(which is our London (GMT) time zone).
In other timezones, the time is always converted to UTC before being
saved into the TIMESTAMP field. On retrieval, the Timestamp will be
converted to the local zone time for the local TimeZone.

-------------------------------------------------------------------------------
SETUP:
Netbeans:
Download and add a MariaDB Connector/J .jar file to Libraries folder in your Project
(e.g. mariadb-java-client-2.5.4.jar)

MariaDB Database Setup:
Use phpMyAdmin (or mysql command line to create an Events
database table, in the 'test' database, using the following SQL.

  CREATE TABLE Events
  (
      id BIGINT PRIMARY KEY AUTO_INCREMENT,
      timestamp TIMESTAMP(3)
  );

TIMESTAMP(3) means 'provide 3 decimal places for seconds' (i.e milliseconds)

NB: Note that we must set the MariaDB attribute :  "useFractionalSeconds=true"
or the database will drop the fractional seconds part.
 */

package com.dkit.gd2.vilandasmorrissey;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

public class JDBCTimestampDemo {

    String url = "jdbc:mariadb://localhost/";
    String dbName = "test?useFractionalSeconds=true";  // NB: note the fractional seconds flag
    String driver = "org.mariadb.jdbc.Driver";
    String userName = "root";
    String password = "";  //  or password = null; if there is no password on your database

    public static void main(String[] args) {
        JDBCTimestampDemo app = new JDBCTimestampDemo();
        app.start();
    }

    public void start() {
        System.out.println("DEMO: Write time Instant to MySQL TIMESTAMP field.");

        try {
            Class.forName(driver);            // load JDBC driver
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (
                Connection conn = DriverManager.getConnection(url + dbName, userName, password);
                Statement stmt = conn.createStatement();
        ) {
            System.out.println("Connected to the database, INSERTing into Events table...");

            // Insert an event row
            String sql = "INSERT INTO Events (id , timestamp ) VALUES( null , ? );";
            PreparedStatement preparedStatement = conn.prepareStatement(sql);

            // Create Instant object from String
            String dateTime = "2020-02-14T10:15:30.123Z"; // possibly read from a file
            Instant inst  = Instant.parse( dateTime );

            // Instant inst = Instant.now();  // get current timestamp (get current moment in time)
            // Instant inst = Instant.now().truncatedTo(ChronoUnit.MILLIS); // if we need only millisecond precision
            // long milliSeconds = inst.toEpochMilli();
            // System.out.println("milliSeconds: "+milliSeconds);

            // Convert from String to Timestamp (note String format is different for conversion to Instant)
            // java.sql.Timestamp ts_now = java.sql.Timestamp.valueOf("2020-02-14 23:45:00.887");

            java.sql.Timestamp ts_now = java.sql.Timestamp.from( inst );
            // Actually in UTC, but its `toString` method applies JVM’s current
            // default time zone while generating string.

            System.out.println("timestamp: "+ ts_now);  // will have milliseconds

            preparedStatement.setTimestamp(1, ts_now );
            preparedStatement.executeUpdate();

            // Query all.
            sql = "SELECT * FROM Events ;";
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                //Retrieve by column name
                long id = rs.getLong("id");     // MySql BIGINT maps to Java long (8-byte value)
                java.sql.Timestamp ts = rs.getTimestamp("timestamp");
                Instant instant = ts.toInstant();  // Same moment, also in UTC.

                System.out.println("id: " + id + " | Instant: " + instant );
            }

            // ZonedDateTime (not needed in TollEvent project)
            // Combine the Instant and a Zone to create a ZonedDateTime (an equivalent DateTime for a paricular Zone)
            //ZoneId z =  ZoneId.of( "America/Montreal" );
            //ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, z);  // get local/default zone id
            //ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());  // get local/default zone id  (In Ireland its UTC or GMT)

            //        System.out.println("id: " + id
            //                                  + "\n | as Instant: " + instant
            //                                  + "\n | as Timestamp: " + ts
            //                                  + "\n | as ZonedDateTime: " + zdt
            //                                  + "\n | as toEpochMilli: " + instant.toEpochMilli() );
            // }

        } catch ( SQLException e ) {
            e.printStackTrace();
        }
    }
}
