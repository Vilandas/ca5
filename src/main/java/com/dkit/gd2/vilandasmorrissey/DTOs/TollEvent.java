package com.dkit.gd2.vilandasmorrissey.DTOs;

import com.dkit.gd2.vilandasmorrissey.BusinessObjects.Enums;

import java.time.Instant;

public class TollEvent implements Comparable<TollEvent>
{
    private String vehicleRegistration;
    private long imageID;
    private Instant time;
    private Enums.Country country;

    public TollEvent(String vehicleRegistration, long imageID, Instant time,
                     Enums.Country country)
    {
        this.vehicleRegistration = vehicleRegistration;
        this.imageID = imageID;
        this.time = time;
        this.country = country;
    }

    public String getVehicleRegistration()
    {
        return vehicleRegistration;
    }

    public long getImageID()
    {
        return imageID;
    }

    public Instant getTime()
    {
        return time;
    }

    public Enums.Country getCountry()
    {
        return country;
    }

    @Override
    public String toString()
    {
        return String.format("%-10s %-16s %-24s %s", imageID, vehicleRegistration, time, country);
    }

    @Override
    public int compareTo(TollEvent other)
    {
        return this.vehicleRegistration.compareTo(other.vehicleRegistration);
    }
}
