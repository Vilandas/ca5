package com.dkit.gd2.vilandasmorrissey.BusinessObjects;

import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;
import com.dkit.gd2.vilandasmorrissey.Daos.ITollsDaoInterface;
import com.dkit.gd2.vilandasmorrissey.Daos.MySQLTollsDAO;
import com.dkit.gd2.vilandasmorrissey.Exceptions.DaoException;

import java.security.CodeSigner;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;

public class Menu
{
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    private static final double THRESHOLD = 0.70; //For input similarity checking

    Scanner keyboard = new Scanner(System.in);
    VehicleRegistrations vehicleRegistrations = new VehicleRegistrations();
    TollEvents tollEvents = new TollEvents();
    String inKey = "\n)> ";

    public void start()
    {
        ITollsDaoInterface ITollsDao = new MySQLTollsDAO();
        boolean run = true;
        String input;
        int number;
        Enums.Menu menuOption;

        while (run)
        {
            printMenu();
            input = keyboard.nextLine();
            if(isInt(input))
            {
                number = Integer.parseInt(input);
            }
            else
            {
                number = getEnumFromString(input);
            }

            if (number < 0 || number > Enums.Menu.values().length - 1)
            {
                System.out.println("\nInvalid input");
                continue;
            }

            menuOption = Enums.Menu.values()[number];
            try
            {
                switch (menuOption)
                {
                    case QUIT:
                        run = false;
                        break;
                    case LOAD_REGS:
                        vehicleRegistrations.loadRegistrations();
                        break;
                    case PROCESS_TOLLS:
                        tollEvents.loadTollEvents(vehicleRegistrations);
                        break;
                    case UPLOAD:
                        uploadTollsToDatabase(ITollsDao);
                        break;
                    case GET_ALL:
                        getAllTollEvents(ITollsDao);
                        break;
                    case GET_REG:
                        getTollsFromReg(ITollsDao);
                        break;
                    case GET_DATE:
                        getTollsFromDate(ITollsDao);
                        break;
                    case GET_DATE_RANGE:
                        getTollsFromDateRange(ITollsDao);
                        break;
                    case GET_UNIQUE:
                        getTollsUniqueReg(ITollsDao);
                        break;
                    case GET_MAP:
                        getAllTollsAsMap(ITollsDao);
                        break;
                    case GET_UNKNOWN:
                        getTollsUnknownCountry(ITollsDao);
                        break;
                    case GET_COUNTRY:
                        getTollsFromCountry(ITollsDao);
                        break;
                    case CLEAR_EVENTS:
                        ITollsDao.clearEvents();
                        break;
                }
            }
            catch(DaoException e)
            {
                System.out.println("DAO Exception: " + e.getMessage());
            }
        }
    }

    /**
     * Display menu options
     */
    private void printMenu()
    {
        System.out.println("\nOptions to select:");
        for (int i = 0; i < Enums.Menu.values().length; i++)
        {
            System.out.println("\t" + ANSI_GREEN + i + "." + Enums.Menu.values()[i] + ANSI_RESET +
                    ": " + Enums.Menu.values()[i].getDesc());
        }
        System.out.print("Enter a number or highlighted option" + inKey);
    }

    /**
     * Check to see if the input is an int
     * @param input User input string
     * @return true if input can be parsed to int
     */
    private boolean isInt(String input)
    {
        if (input == null)
        {
            return false;
        }
        try
        {
            Integer.parseInt(input);
        } catch (NumberFormatException e)
        {
            return false;
        }
        return true;
    }

    /**
     * Returns the most similar menu option compared to the input
     * Only works for matching words
     * (e.g input = "date ra", "ra" doesn't match any words so will only compare "date")
     * @param input User input string
     * @return -1 if input is not similar to any option, otherwise return the option most similar
     */
    private int getEnumFromString(String input)
    {
        if(input == null)
        {
            return -1;
        }
        input = input.toUpperCase();
        List<Double> menuSimilarity = new ArrayList();
        HashMap<String, Integer> inputWords = new HashMap();

        //Add words and frequency of them from input to a HashMap
        for(String word : input.split(" "))
        {
            int count = inputWords.containsKey(word) ? inputWords.get(word) : 0;
            inputWords.put(word, count + 1);
        }

        //For each menu option
        for(int i = 0; i < Enums.Menu.values().length; i++)
        {
            HashMap<String, Integer> menuWords = new HashMap();
            List<Integer> vector1 = new ArrayList();
            List<Integer> vector2 = new ArrayList();

            //Get all words in menu
            for (String word : Enums.Menu.values()[i].toString().toUpperCase().split(" "))
            {
                int count = menuWords.containsKey(word) ? menuWords.get(word) : 0;
                menuWords.put(word, count + 1);
            }

            //Add frequency of menuWords to vector1
            //Add frequency of menuWords in inputWords to vector2
            for(String word : menuWords.keySet())
            {
                int count = menuWords.get(word);
                vector1.add(count);

                count = inputWords.containsKey(word) ? inputWords.get(word) : 0;
                vector2.add(count);
            }
            menuSimilarity.add(cosineSimilarity(vector1, vector2));
        }
        //Return highest index or if equal, first instance
        int highest = 0;
        for(int i = 1; i < menuSimilarity.size(); i++)
        {
            highest = menuSimilarity.get(i) > menuSimilarity.get(highest) ? i : highest;
        }
        return highest >= THRESHOLD ? highest : -1;
    }

    //Referenced from https://stackoverflow.com/questions/1746501/can-someone-give-an-example-of-cosine-similarity-in-a-very-simple-graphical-wa?rq=1
    /**
     * Method to calculate cosine similarity of vectors
     * 1 - exactly similar (angle between them is 0)
     * 0 - orthogonal vectors (angle between them is 90)
     * @param vector1 - vector in the form [a1, a2, a3, ..... an]
     * @param vector2 - vector in the form [b1, b2, b3, ..... bn]
     * @return - the cosine similarity of vectors (ranges from 0 to 1)
     */
    private double cosineSimilarity(List<Integer> vector1, List<Integer> vector2) {
        if(vector1 == null || vector2 == null)
        {
            return 0;
        }
        double dotProduct = 0;
        double normA = 0;
        double normB = 0;
        for (int i = 0; i < vector1.size(); i++)
        {
            dotProduct += vector1.get(i) * vector2.get(i);
            normA += Math.pow(vector1.get(i), 2);
            normB += Math.pow(vector2.get(i), 2);
        }
        double output = dotProduct / (Math.sqrt(normA) * Math.sqrt(normB));
        return Double.isNaN(output) ? 0 : output;
    }

    private void uploadTollsToDatabase(ITollsDaoInterface ITollsDao) throws DaoException
    {
        ITollsDao.uploadTollEvents(tollEvents);
    }

    private void getAllTollEvents(ITollsDaoInterface ITollsDao) throws DaoException
    {
        List<TollEvent> tollList = ITollsDao.getAllTollEvents();
        printTollList(tollList);
    }

    private void getTollsFromReg(ITollsDaoInterface ITollsDao) throws DaoException
    {
        System.out.print("\nEnter a registry to search for" + inKey);
        String input = keyboard.nextLine();

        List<TollEvent> tollList = ITollsDao.getTollsFromReg(input);
        printTollList(tollList);
    }

    private void getTollsFromDate(ITollsDaoInterface ITollsDao) throws DaoException
    {
        try
        {
            System.out.println("\nFormat 1: YYYY-MM-DD");
            System.out.println("Format 2: YYYY-MM-DD hh:mm:ss");
            System.out.print("\nEnter a start date in either format 1 or format 2" + inKey);
            Instant startDate = getInstantFromString(keyboard.nextLine());

            List<TollEvent> tollList = ITollsDao.getTollsFromDateRange(startDate, Instant.now());
            printTollList(tollList);
        }
        catch(DateTimeParseException e)
        {
            System.out.println("Could not translate date");
        }
    }

    private void getTollsFromDateRange(ITollsDaoInterface ITollsDao) throws DaoException
    {
        try
        {
            System.out.println("\nFormat 1: YYYY-MM-DD");
            System.out.println("Format 2: YYYY-MM-DD hh:mm:ss");
            System.out.print("Enter a start date in either format 1 or format 2" + inKey);
            Instant startDate = getInstantFromString(keyboard.nextLine());

            System.out.println("\nFormat 1: YYYY-MM-DD");
            System.out.println("Format 2: YYYY-MM-DD hh:mm:ss");
            System.out.print("Enter a end date in either format 1 or format 2" + inKey);
            Instant endDate = getInstantFromString(keyboard.nextLine());
            List<TollEvent> tollList = ITollsDao.getTollsFromDateRange(startDate, endDate);
            printTollList(tollList);
        }
        catch(DateTimeParseException e)
        {
            System.out.println("Could not translate date");
        }
    }

    /**
     * Convert string (date or date and time) to Instant date and time
     * @param input user input date string
     * @return Date and time in Instant format
     * @throws DateTimeParseException
     */
    public Instant getInstantFromString(String input) throws DateTimeParseException
    {
        Instant instantDate;
        if(input.length() > 11)
        {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime dateTime = LocalDateTime.from(formatter.parse(input));
            instantDate = dateTime.atZone(ZoneId.of("UTC")).toInstant();
        }
        else
        {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate date = LocalDate.from(formatter.parse(input));
            instantDate = date.atStartOfDay(ZoneId.of("UTC")).toInstant();
        }
        return instantDate;
    }

    private void getTollsUniqueReg(ITollsDaoInterface ITollsDao) throws DaoException
    {
        List<TollEvent> tollList = ITollsDao.getTollsUniqueReg();
        Collections.sort(tollList);
        printTollList(tollList);
    }

    private void getAllTollsAsMap(ITollsDaoInterface ITollsDao) throws DaoException
    {
        Map<String, List<TollEvent>> tollEventsMap = ITollsDao.getAllTollsAsMap();

        System.out.println(String.format("%-10s %-16s %-24s %s", "ImageID:", "Registration:", "Time:", "Country:"));
        for(String registry : tollEvents.keySet())
        {
            for(TollEvent toll : tollEventsMap.get(registry))
            {
                System.out.println(toll);
            }
        }
    }

    private void getTollsUnknownCountry(ITollsDaoInterface ITollsDao) throws DaoException
    {
        List<TollEvent> tollList = ITollsDao.getTollsFromCountry(Enums.Country.UNKNOWN);
        printTollList(tollList);
    }

    private void getTollsFromCountry(ITollsDaoInterface ITollsDao) throws DaoException
    {
        System.out.print("\nEnter a country to search for" + inKey);
        String input = keyboard.nextLine().toUpperCase();

        try
        {
            Enums.Country country = Enums.Country.valueOf(input);
            List<TollEvent> tollList = ITollsDao.getTollsFromCountry(country);
            printTollList(tollList);
        }
        catch(IllegalArgumentException e)
        {
            System.out.println("No country of that type found");
        }
    }

    private void printTollList(List<TollEvent> tolls)
    {
        System.out.println(String.format("%-10s %-16s %-24s %s", "ImageID:", "Registration:", "Time:", "Country:"));
        for(TollEvent toll : tolls)
        {
            System.out.println(toll);
        }
    }
}
