package com.dkit.gd2.vilandasmorrissey.BusinessObjects;

import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;

import java.util.Comparator;

public class TollEventComparatorByImageID implements Comparator<TollEvent>
{
    @Override
    public int compare(TollEvent e1, TollEvent e2)
    {
        return Long.compare(e1.getImageID(), e2.getImageID());
    }
}
