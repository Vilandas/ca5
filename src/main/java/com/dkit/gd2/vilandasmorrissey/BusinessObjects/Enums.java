package com.dkit.gd2.vilandasmorrissey.BusinessObjects;

public class Enums
{
    public enum Menu
    {
        QUIT("Quit application"),
        LOAD_REGS("Load valid vehicle registrations from file"),
        PROCESS_TOLLS("Process all toll events from file into a data structure"),
        UPLOAD("Write toll events from data structure to database"),
        GET_ALL("Display all toll events from database"),
        GET_REG("Display all toll events from registry input"),
        GET_DATE("Display all toll events from input date to current date"),
        GET_DATE_RANGE("Display all toll events from the range input start date to input end date"),
        GET_UNIQUE("Display all toll events with no duplicate registrations and in alphabetical order"),
        GET_MAP("Get all toll events in map form and display"),
        GET_UNKNOWN("Get all toll events with unknown country"),
        GET_COUNTRY("Get all toll events from country input"),
        CLEAR_EVENTS("Clear the database of all events");

        private final String description;

        Menu(String description)
        {
            this.description = description;
        }

        public String getDesc()
        {
            return description;
        }

        @Override
        public String toString()
        {
            String name = name();
            name = name.replaceAll("_", " ");
            return name.charAt(0) + name.substring(1).toLowerCase();
        }
    }

    public enum Country
    {
        IRELAND,
        NORTHERN_IRELAND,
        UNITED_KINGDOM,
        SPAIN,
        UNKNOWN;

        @Override
        public String toString()
        {
            String name = name();
            name = name.replaceAll("_", " ");
            return name.charAt(0) + name.substring(1).toLowerCase();
        }
    }
}
