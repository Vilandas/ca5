package com.dkit.gd2.vilandasmorrissey.BusinessObjects;

import com.dkit.gd2.vilandasmorrissey.DTOs.TollEvent;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class TollEvents implements Map<String, HashSet<TollEvent>>
{
    private HashMap<String, HashSet<TollEvent>> tollEvents = new HashMap();

    public void loadTollEvents(VehicleRegistrations registrations)
    {
        tollEvents.clear();
        try(Scanner scanner = new Scanner(new BufferedReader(new FileReader("Toll-Events.csv"))))
        {
            scanner.useDelimiter(";|\\r\\n");
            while(scanner.hasNext())
            {
                String registration = scanner.next();
                //If next line is not blank (whitespace)
                if(!registration.isBlank())
                {
                    //If look-up table doesn't contain registration, add new one with country code
                    if(!registrations.containsKey(registration))
                    {
                        registrations.put(registration, getRegistrationCountry(registration));
                    }
                    long imageID = scanner.nextLong();
                    Instant time = Instant.parse(scanner.next()).truncatedTo(ChronoUnit.SECONDS);
                    Enums.Country country = registrations.get(registration);

                    TollEvent event = new TollEvent(
                            registration, imageID, time, country);

                    //If registration already has a toll, just add new event
                    //Otherwise create new event set for tolls associated with that registry
                    if(tollEvents.containsKey(registration))
                    {
                        tollEvents.get(registration).add(event);
                    }
                    else
                    {
                        HashSet<TollEvent> eventSet = new HashSet();
                        eventSet.add(event);
                        tollEvents.put(registration, eventSet);
                    }
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    //Scan through all country code regex to determine country or unknown
    private Enums.Country getRegistrationCountry(String registration)
    {
        for(Enums.Country country : CountryCodes.codes().keySet())
        {
            if(registration.matches(CountryCodes.codes().get(country)))
            {
                return country;
            }
        }
        return Enums.Country.UNKNOWN;
    }
    
    @Override
    public int size()
    {
        return tollEvents.size();
    }

    @Override
    public boolean isEmpty()
    {
        return tollEvents.isEmpty();
    }

    @Override
    public boolean containsKey(Object key)
    {
        return tollEvents.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value)
    {
        return tollEvents.containsValue(value);
    }

    @Override
    public HashSet<TollEvent> get(Object key)
    {
        return tollEvents.get(key);
    }

    @Override
    public HashSet<TollEvent> put(String key, HashSet<TollEvent> value)
    {
        return tollEvents.put(key, value);
    }

    @Override
    public HashSet<TollEvent> remove(Object key)
    {
        return tollEvents.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends HashSet<TollEvent>> m)
    {
        tollEvents.putAll(m);
    }

    @Override
    public void clear()
    {
        tollEvents.clear();
    }

    @Override
    public Set<String> keySet()
    {
        return tollEvents.keySet();
    }

    @Override
    public Collection<HashSet<TollEvent>> values()
    {
        return tollEvents.values();
    }

    @Override
    public Set<Entry<String, HashSet<TollEvent>>> entrySet()
    {
        return tollEvents.entrySet();
    }
}
