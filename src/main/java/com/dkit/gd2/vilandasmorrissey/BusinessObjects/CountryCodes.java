package com.dkit.gd2.vilandasmorrissey.BusinessObjects;

import java.util.HashMap;

public final class CountryCodes
{
    private static HashMap<Enums.Country, String> countryCodes;
    static
    {
        //IRELAND: https://en.wikipedia.org/wiki/Vehicle_registration_plates_of_the_Republic_of_Ireland
        countryCodes = new HashMap();
        String regex = "(^[0189][0-9]|^[123][0-9][12])" +
                "(C[ENW]?|D[L]?|G|K[EKY]|L[DHMSK]?|M[HNO]|OY|RN|SO|T[NS]?|W[HXWD]?)" +
                "\\d{1,6}";
        countryCodes.put(Enums.Country.IRELAND, regex);

        //UK and Northern Ireland regex where not made by me. NI has conflicting information on registration plates.
        //UNITED_KINGDOM: https://www.regextester.com/97133
        //https://en.wikipedia.org/wiki/Vehicle_registration_plates_of_the_United_Kingdom
        regex = "(^[A-Z]{2}[0-9]{2}[A-Z]{3}$)|(^[A-Z][0-9]{1,3}[A-Z]{3}$)|(^[A-Z]{3}[0-9]{1,3}[A-Z]$)|" +
                "(^[0-9]{1,4}[A-Z]{1,2}$)|(^[0-9]{1,3}[A-Z]{1,3}$)|(^[A-Z]{1,2}[0-9]{1,4}$)|" +
                "(^[A-Z]{1,3}[0-9]{1,3}$)";
        countryCodes.put(Enums.Country.UNITED_KINGDOM, regex);

        //NORTHERN_IRELAND
        //http://www.regexlib.com/REDetails.aspx?regexp_id=527&AspxAutoDetectCookieSupport=1
        regex = "[A-CEGHJ-PR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{2}[0-9]{2}[0-9]{2}[A-DFMP]";
        countryCodes.put(Enums.Country.NORTHERN_IRELAND, regex);

        //SPAIN
        regex = "^[CEHPRSTV]?[0-9]{4}[BCDFGHJKLMNPRSTVWXYZ]{3}";
        countryCodes.put(Enums.Country.SPAIN, regex);
    }

    public static HashMap<Enums.Country, String> codes(){return countryCodes;}
}
