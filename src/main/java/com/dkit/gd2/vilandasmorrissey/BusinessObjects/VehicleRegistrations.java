package com.dkit.gd2.vilandasmorrissey.BusinessObjects;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class VehicleRegistrations implements Map<String, Enums.Country>
{
    private HashMap<String, Enums.Country> registrations = new HashMap();

    public void loadRegistrations()
    {
        try(Scanner scanner = new Scanner(new BufferedReader(new FileReader("Vehicles.csv"))))
        {
            scanner.useDelimiter("\\r");
            while(scanner.hasNext())
            {
                String registration = scanner.next();
                if(!registrations.containsKey(registration))
                {
                    registrations.put(registration, getRegistrationCountry(registration));
                }
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    //Scan through all country code regex to determine country or unknown
    private Enums.Country getRegistrationCountry(String registration)
    {
        for(Enums.Country country : CountryCodes.codes().keySet())
        {
            if(registration.matches(CountryCodes.codes().get(country)))
            {
                return country;
            }
        }
        return Enums.Country.UNKNOWN;
    }

    @Override
    public int size()
    {
        return registrations.size();
    }

    @Override
    public boolean isEmpty()
    {
        return registrations.isEmpty();
    }

    @Override
    public boolean containsKey(Object key)
    {
        return registrations.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value)
    {
        return registrations.containsValue(value);
    }

    @Override
    public Enums.Country get(Object key)
    {
        return registrations.get(key);
    }

    @Override
    public Enums.Country put(String key, Enums.Country value)
    {
        return registrations.put(key, value);
    }

    @Override
    public Enums.Country remove(Object key)
    {
        return registrations.remove(key);
    }

    @Override
    public void putAll(Map<? extends String, ? extends Enums.Country> m)
    {
        registrations.putAll(m);
    }

    @Override
    public void clear()
    {
        registrations.clear();
    }

    @Override
    public Set<String> keySet()
    {
        return registrations.keySet();
    }

    @Override
    public Collection<Enums.Country> values()
    {
        return registrations.values();
    }

    @Override
    public Set<Entry<String, Enums.Country>> entrySet()
    {
        return registrations.entrySet();
    }
}
